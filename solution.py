def rev(n):
    r= str(n)[::-1]
    return int(r)

def nextPalindrome(n):
    while(n!=rev(n)):
        n+=1
    return n
#inp=[3,546,6577,76866]
inp = list(map(int, input().split(' ')))
t= inp[0]
for i in range(1, len(inp)):
    print(nextPalindrome(inp[i]))


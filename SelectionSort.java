
// It is a solution of hacker rank selection sort problem.

import java.util.*;
class TestClass {
    public static void main(String args[] ) throws Exception {
        /* Sample code to perform I/O:
         * Use either of these methods for input

        //BufferedReader
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String name = br.readLine();                // Reading input from STDIN
        System.out.println("Hi, " + name + ".");    // Writing output to STDOUT

       

        */

        // Write your code here
         //Scanner
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int itr = sc.nextInt();
        int arr [] = new int[n];
        int c=0;
        for(int i=0; i<n;i++){
            arr[i]=sc.nextInt();
            
        }
         
         int minimum;        
        // reduces the effective size of the array by one in  each iteration.

        for(int i = 0; i < n-1 ; i++)  {

           // assuming the first element to be the minimum of the unsorted array .
             minimum = i ;

          // gives the effective size of the unsorted  array .

            for(int j = i+1; j < n ; j++ ) {
                if(arr[ j ] < arr[ minimum ])  {                //finds the minimum element
                minimum = j ;
                }
             }
          // putting minimum element on its proper position.
          //swap ( A[ minimum ], A[ i ]) ; 
          int temp= arr[i];
          arr[i] = arr[minimum];
          arr[minimum] = temp;
          c++;
          if(c== itr){
              break;
          }
        
        }
        for(int i=0; i<n;i++){
           System.out.printf(arr[i]+" ");
            
        }
        

    }
}

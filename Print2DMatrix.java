public class Print2DMatrix {
	
	public static void print2Dmatrix(int[][] matrix, int start, int end) {
		//int len = matrix.length - start;
		for (int i = start; i < end; i++) {
			System.out.printf(matrix[start][i]+" ");
		}
		for (int i = start+1; i < end; i++) {
			System.out.printf(matrix[i][end-1]+" ");
		}
		for (int i =end-2; i >=start; i--) {
			System.out.printf(matrix[end-1][i]+" ");
		}
		for (int i = end-2; i >start; i--) {
			System.out.printf(matrix[i][start]+" ");
		}
		if((end-start)/2==1) {
			return;
		}
		System.out.println();
		print2Dmatrix(matrix, ++start,--end);
	}

	public static void main(String[] args) {
		int [][] matrix = new int[][] { {1,2,3,4},{5,6,7,8}, {9,10,11,12}, {13,14,15,16}};
		// TODO Auto-generated method stub
		print2Dmatrix(matrix, 0, matrix.length);
		
	}

}